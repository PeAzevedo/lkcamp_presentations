.. role:: cover-title

.. role:: cover

:cover-title:`PCI-Express`
#############################

.. space:: 150

.. class:: cover

  A introduction to the software side of PCI-E.

.. raw:: pdf

   PageBreak standardPage


What is PCI-Express?
====================

* PCI-Express stands for Peripheral Component Interconnect Express.
* Is a specification for a high performance serial IO to interconnect peripherals.
* It Allows Peer to Peer comunication between devices, Hot plug and Interruption
  between the device and host.
* Evolved from PCI and PCI-X.
* Software Backcompatible to the PCI, PCI-X and other versions of PCI-E.
* Developed by Intel, Dell, HP, IBM and mainteined by PCI-SIG[1].


How it generally looks like
===========================

.. image:: pci_imgs/PCI_und_PCIe_Slots.jpg
   :width: 10cm

Source: https://en.wikipedia.org/wiki/File:PCI_und_PCIe_Slots.jpg

.. raw:: pdf

   PageBreak coverPage

:cover-title:`Architecture`
===========================

* Link specification
* PCI-E Address Spaces
* Topology

.. raw:: pdf

   PageBreak standardPage


Link specification
==================

* **Lane**
   * It is a pair of dual simplex.
   * Each lane can send and receive data at same time.
   * Each lane of PCI-E is full duplex.
* **Link**
   * A link is a set of lanes.
   * A link can have between 1 and 32 lanes.
   * Called 1x, 2x, ... 32x.

.. image:: pci_imgs/PCI_Express_Terminology.png
   :width: 11cm

Source: https://en.wikipedia.org/wiki/File:PCI_Express_Terminology.svg


PCI-E Link Throughput
=====================

+---------------+---------------+---------------------------+
|               |               |         Throughput        |
|    Version    | Released date +-------------+-------------+
|               |               |      x1     |     x16     |
+---------------+---------------+-------------+-------------+
|      1.0      |      2003     |     250     |   4.0 GB/s  |
|               |               |     MB/s    |             |
+---------------+---------------+-------------+-------------+
|      2.0      |      2007     |   500 MB/s  |   8.0 GB/s  |
+---------------+---------------+-------------+-------------+
|      3.0      |      2010     |  984.6 MB/s | 15.754 GB/s |
+---------------+---------------+-------------+-------------+
|      4.0      |      2017     | 1969.2 MB/s | 31.508 GB/s |
+---------------+---------------+-------------+-------------+
|      5.0      |      2019     | 3938.5 MB/s | 63.015 GB/s |
+---------------+---------------+-------------+-------------+
| 6.0 (planned) |      2021     |  7.877 GB/s | 126.03 GB/s |
+---------------+---------------+-------------+-------------+

Source: https://en.wikipedia.org/wiki/PCI_Express#History_and_revisions


PCI-E Address Spaces
====================

* The PCI-E device have several address spaces

   * Configuration space
   * Memory Address space(Optional)
   * IO address space(Optional)

      * A.K.A. Port IO

   * Message space


PCI-E configuration space
=========================

* It is a block of memory(4kb) in the PCI-E device.
* Contains all information necessary to identify and configure a PCI-E device.
* Have a PCI-Compatible configuration space too, for software backward
  compatibility.
* Every Device function has to have a configuration space.

.. raw:: pdf

   PageBreak standardPage

* Contain information like Vendor/product/subsystem ID.

   * This information is used to load the suitable driver.
   * Ex (VendorID/productID):

      * Intel UHD Graphics:                        0x8086/0x9bc4
      * AMD/ATI Bonaire XTX [Radeon R7 260X/360] : 0x1002/0x6658

* BAR (Base Address Register).

   * Are 32bits register that are used for resource mapping to the main memory.


PCI configuration space
=========================

.. image:: pci_imgs/PCI_config_space.png
   :width: 13cm

Source: https://lwn.net/Kernel/LDD3/


Memory Address space(Optional)
==============================

* Access PCI-compatible and PCI-E configuration spaces.
* Map the device resources to the system memory.

   * MMIO(Memory Mapped IO).
   * Resources like GPU VRAM.

* Each BAR register points to a address in the system memory that a PCI-E device resource is mapped.


IO address space(Optional)
==========================

* Can't access the PCI-E configuration space.
* Architecture dependent.
* Its use is discouraged.
* Only used in specific scenario nowdays.

   * Ex: Only way to set a specific register(PCIXBAR[intel]) to enable MMIO by BIOS.

.. raw:: pdf

    PageBreak

* Uses CPU's special instructions in special addresses to access PCI resources like:

   * PCI-compatible configuration(s) space(s).
   * PCI end-point device.


Message space
==============

* Used for low-level protocol messaging/interrupt.


Topology
========

* This describes how a device is located in the PCI enviroment.
* The concept of bus, Device and function is used to identify a PCI Device.

   * **bus**: 8 bits, up to 256 Buses.
   * **Device**: 5 bits, up to 32 Devices per bus.
   * **Function**: 3 bits, up to 8 Functions per device.

.. raw:: pdf

   PageBreak

* The "main" bus is the bus 0 and it will always exist.

   * Consists of a Virtual PCI bus with integrated endpoints and Virtual PCI-to-PCI
     Bridges which are hard-coded with a Device number and Function number.

* A PCI enviroment is composed of some components like:
* **Root complex**.

   * Very vague definition.
   * Connect the CPU to the Memory controller and PCI Express switch fabric.
   * Commonly assined to the bus 0.

.. raw:: pdf

   PageBreak


* **IO Controller Hub(ICH/FCH/PCH/SouthBridge)**.

    * Conects to the NorthBridge/Uncore.
    * Provides features like SATA controller, USB Controller, AHCI, SPI, etc.

* **Bridges and Switches**

   * A device in the PCI enviroment.
   * Connects two or more buses.

.. image:: pci_imgs/northsouth2.jpg
   :width: 11cm

Source: https://csmmtssr.wordpress.com/2011/09/14/northbridge-and-southbridge-in-detail/

.. image:: pci_imgs/Topology_1.png
   :width: 15cm

Source: https://csmmtssr.wordpress.com/2011/09/14/northbridge-and-southbridge-in-detail/

.. image:: pci_imgs/Topology_2.png
   :Height: 9cm

Source: https://pcisig.com/sites/default/files/files/PCI_Express_Basics_Background.pdf

Example of Topology
===================

.. code-block::

   $lspci -t -v
   -[0000:00]-+-00.0  Intel Corporation 4th Gen Core Processor DRAM Controller
              +-01.0-[01]--+-00.0  Advanced Micro Devices, Inc. [AMD/ATI] Bonaire XTX [Radeon R7 260X/360]
              |            \-00.1  Advanced Micro Devices, Inc. [AMD/ATI] Tobago HDMI Audio [Radeon R7 360 / R9 360 OEM]
              +-02.0  Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
              +-03.0  Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller
              +-14.0  Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI
              +-16.0  Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1
              +-16.3  Intel Corporation 8 Series/C220 Series Chipset Family KT Controller
              +-1a.0  Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2
              +-1b.0  Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller
              +-1c.0-[02]--
              +-1c.1-[03-04]----00.0-[04]--
              +-1c.2-[05]----00.0  Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
              +-1d.0  Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1
              +-1f.0  Intel Corporation B85 Express LPC Controller
              +-1f.2  Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]
              \-1f.3  Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller

.. raw:: pdf

   PageBreak coverPage

:cover-title:`PCI-E Linux kernel subsystem`
===========================================

.. raw:: pdf

   PageBreak standardPage


PCI-E kernel and BIOS onus
==========================

* The BIOS is responsible to enumerate all devices in the enviroment.
* The kernel PCI Bus is responsible match the PCI/PCI-E devices to the suitable
  registered driver.


How access PCI-E resources on linux
===================================

* **Register the device in the PCI Framework**:

.. code-block:: c

   include/linux/pci.h

   #define pci_register_driver(driver)		\
           __pci_register_driver(driver, THIS_MODULE, KBUILD_MODNAME)

.. code-block:: c

    drivers/pci/pci-driver.c

    int __pci_register_driver(struct pci_driver *drv, struct module *owner,
                              const char *mod_name)


The struct pci_driver & pci_dev
===============================

.. code-block:: c

    include/linux/pci.h

    struct pci_driver {
        struct list_head	node;
        const char		*name;
        const struct pci_device_id *id_table;
        int  (*probe)(struct pci_dev *dev, const struct pci_device_id *id);
        void (*remove)(struct pci_dev *dev);
        int  (*suspend)(struct pci_dev *dev, pm_message_t state);
        int  (*resume)(struct pci_dev *dev);
        void (*shutdown)(struct pci_dev *dev);
        int  (*sriov_configure)(struct pci_dev *dev, int num_vfs);
        const struct pci_error_handlers *err_handler;
        const struct attribute_group **groups;
        struct device_driver	driver;
        struct pci_dynids	dynids;
    };


.. raw:: pdf

    PageBreak

.. code-block:: c

    include/linux/pci.h

    /* The pci_dev structure describes PCI devices */
    struct pci_dev {
        [...]
        unsigned int	devfn;		/* Encoded device & function index */
        unsigned short	vendor;
        unsigned short	device;
        unsigned short	subsystem_vendor;
        unsigned short	subsystem_device;
        unsigned int	class;
        [...]
    }


Note worth pci_driver members
=============================

* **probe()**:

.. code-block:: c

    int  (*probe)(struct pci_dev *dev, const struct pci_device_id *id);

* This is called when a device that your drive supports is found by the
  PCI framework.
* Commonly used to initialize the driver.

* **remove()**:

.. code-block:: c

    void (*remove)(struct pci_dev *dev);

.. raw:: pdf

    PageBreak

* **struct id_table**:

.. code-block:: c

    const struct pci_device_id *id_table;

* Used to inform the framework which devices your driver support.
* It uses the vendor/device ID in Configuration space of the PCI device
  to determine if your driver is suitable.

.. code-block:: c

    mod_devicetable.h

    struct pci_device_id {
        __u32 vendor, device;		/* Vendor and device ID or PCI_ANY_ID*/
        __u32 subvendor, subdevice;	/* Subsystem ID's or PCI_ANY_ID */
        __u32 class, class_mask;	/* (class,subclass,prog-if) triplet */
        kernel_ulong_t driver_data;	/* Data private to the driver */
    };


Setup and Configuration Read/Write
==================================

* **Enabling the device**:

.. code-block:: c

    drivers/pci/pci.c

    int pci_enable_device(struct pci_dev *dev);

* It wakes up the device and in some cases also assigns its interrupt line and I/O regions

.. raw:: pdf

    PageBreak

* **Reding the configuration space**:

.. code-block:: c

    linux/pci.h

        int pci_bus_read_config_byte(struct pci_bus *bus, unsigned int devfn,
                                     int where, u8 *val);
        int pci_bus_read_config_word(struct pci_bus *bus, unsigned int devfn,
                                     int where, u16 *val);

* The `where` argument is the byte offset from the beginning of the configuration space.
* The value fetched from the configuration space is returned through the `val` pointer.
* `Devfn` encodes number of PCI slot in which the desired PCI device resides and the
  logical device number within that slot in case of multi-function devices.

.. raw:: pdf

   PageBreak

* **Writing the configuration space**:

.. code-block:: c

    linux/pci.h

        int pci_bus_write_config_byte(struct pci_bus *bus, unsigned int devfn,
                                      int where, u8 val);
        int pci_bus_write_config_word(struct pci_bus *bus, unsigned int devfn,
                                      int where, u16 val);

* The `val` argument is the value to be written.


Writing/read from/to PCI-E device
=================================

* **Reserve the PCI region associated with the BAR**:

.. code-block:: c

    drivers/pci/pci.c

    int pci_request_region(struct pci_dev *pdev, int bar, const char *res_name)

* **Take the __iomem from the selected region**:

.. code-block:: c

    lib/pci_iomap.c

    void __iomem *pci_iomap(struct pci_dev *dev, int bar, unsigned long maxlen)

.. raw:: pdf

   PageBreak

* **Use the ioread and write operations**:

.. code-block:: c

    lib/pci_iomap.c

    unsigned int ioread32(void __iomem *addr)
    void iowrite32(u32 datum, void __iomem *addr)

Hotplug
=======

.. code-block:: c

   i915_pci.c

   MODULE_DEVICE_TABLE(pci, pciidlist);

.. raw:: pdf

   PageBreak coverPage

:cover-title:`i915 Example`
===========================

.. raw:: pdf

   PageBreak standardPage


i915
====

* **The registry.**

.. code-block:: c

   i915_pci.c

   err = pci_register_driver(&i915_pci_driver);

* **i915_pci_driver sruct**

.. code-block:: c

  i915_pci.c
  static struct pci_driver i915_pci_driver = {
      .name = DRIVER_NAME,
      .id_table = pciidlist,
      .probe = i915_pci_probe,
      .remove = i915_pci_remove,
      .driver.pm = &i915_pm_ops,
  };

* **The PCI devices supported.**

.. code-block:: c

   i915_pci.c

   static const struct pci_device_id pciidlist[] = {
         [...]
         INTEL_KBL_GT4_IDS(&kbl_gt3_info),
         [...]
         {0, 0, 0}
   };

.. code-block:: c

   i915_pciids.h

   #define INTEL_KBL_GT4_IDS(info) \
           INTEL_VGA_DEVICE(0x593B, info) /* Halo GT4 */

   #define INTEL_VGA_DEVICE(id, info) {		\
          0x8086,	id,				\
          ~0, ~0,					\
          0x030000, 0xff0000,			\
          (unsigned long) info }


Future work
===========

* Protocol

    * Protocol Layers

        * Transaction layer
        * Data link layer
        * Logical phisical layer

    * Packets
    * Routing

.. raw:: pdf

    PageBreak

* Interrupt
* PCI-E HotPlug

   * Physical layer
   * Linux API
* Features


License
=======

This work is licensed under a `Creative Commons "Attribution-ShareAlike 4.0
International"`_ license.

.. _Creative Commons "Attribution-ShareAlike 4.0 International":
   https://creativecommons.org/licenses/by-sa/4.0/deed.en


References
==========

* Linux device drivers, 3rd edition. https://lwn.net/Kernel/LDD3/.
* https://pcisig.com/
* `Wikipedia PCI-E <https://en.wikipedia.org/wiki/pci_express>`_
* `Wikipedia PCI <https://en.wikipedia.org/wiki/peripheral_component_interconnect>`_
* `PCI-SIG slides <https://web.archive.org/web/20140715120034/http://www.pcisig.com/developers/main/training_materials/get_document?doc_id=4e00a39acaa5c5a8ee44ebb07baba982e5972c67>`_
* `PCI-E technology 3.0 book <https://www.mindshare.com/books/titles/pci_express_technology_3.0>`_
* `Understanding pci address mapping <https://stackoverflow.com/questions/37901128/understanding-pci-address-mapping>`_
* `Intel 8th gen datasheet <https://www.intel.com/content/www/us/en/products/docs/processors/core/8th-gen-core-family-datasheet-vol-2.html>`_
* https://www.youtube.com/watch?v=ihgmcp2353i


Other links
===========

* `What is the base address register in pcie <https://stackoverflow.com/questions/30190050/what-is-the-base-address-register-bar-in-pcie>`_
* `How is a pcie bar size determined <https://stackoverflow.com/questions/19006632/how-is-a-pci-pcie-bar-size-determined>`_
* `In/Out instructions in x86 <https://stackoverflow.com/questions/3215878/what-are-in-out-instructions-in-x86-used-for>`_
* `PCI Module example <https://github.com/cirosantilli/linux-kernel-module-cheat/blob/366b1c1af269f56d6a7e6464f2862ba2bc368062/kernel_module/pci.c>`_

